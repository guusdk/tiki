{* $Id$ *}<!DOCTYPE html>
<html lang="{if !empty($pageLang)}{$pageLang}{else}{$prefs.language}{/if}"{if !empty($page_id)} id="page_{$page_id}"{/if}>
<head>
	{include file='header.tpl'}
</head>
<body{html_body_attributes}>

{* Index we display a wiki page here *}
{if $prefs.feature_bidi eq 'y'}
<div dir="rtl">
{/if}
{if $prefs.feature_ajax eq 'y'}
	{include file='tiki-ajax_header.tpl'}
{/if}
{if $is_slideshow eq 'y'}
	<div class="reveal">
		<div class="slides">
{else}
	<div id="main">
		<div id="tiki-center">
			<div id="role_main">
{/if}

{$mid_data}
{if $is_slideshow eq 'y'}
	</div>
		<div id="ss-settings-holder" title="Click for slideshow operations"><span class="fa fa-cogs" style="font-size:1.5rem;color:#666" id="ss-settings"></span></div>
		<div id="ss-options" class="d-flex flex-row justify-content-around align-content-end flex-wrap">
			<div class="p-2">
				<select id="showtheme" class="form-control">
					<option value="">{tr}Change Theme{/tr}</option>
					<option value="black">{tr}Black: Black background, white text, blue links{/tr}</option>
					<option value="blood">{tr}Blood: Dark gray background, dark text, maroon links{/tr}</option>
					<option value="beige">{tr}Beige: Beige background, dark text, brown links{/tr}</option>
					<option value="league">{tr}League: Gray background, white text, blue links{/tr}</option>
					<option value="moon">{tr}Moon: Navy blue background, blue links{/tr}</option>
					<option value="night">{tr}Night: Black background, thick white text, orange links{/tr}</option>
					<option value="serif">{tr}Serif: Cappuccino background, gray text, brown links{/tr}</option>
					<option value="simple">{tr}Simple: White background, black text, blue links{/tr}</option>
					<option value="sky">{tr}Sky: Blue background, thin dark text, blue links{/tr}</option>
					<option value="solarized">{tr}Solarized: Cream-colored background, dark green text, blue links{/tr}</option>
				</select>
			</div>
			<div class="p-2">
				<select id="showtransition" class="form-control">
					<option value="">{tr}Change Transition{/tr}</option>
					<option value="zoom">{tr}Zoom{/tr}</option>
					<option value="fade">{tr}Fade{/tr}</option>
					<option value="slide">{tr}Slide{/tr}</option>
					<option value="convex">{tr}Convex{/tr}</option>
					<option value="concave">{tr}Concave{/tr}</option>
					<option value="">{tr}Off{/tr}</option>
				</select>
			</div>
			{if $prefs.feature_slideshow_pdfexport eq 'y'}
				<div class="p-2"><a href="tiki-slideshow.php?page={$page}&pdf=1&landscape=1" target="_blank" id="exportPDF"><span class="fa fa-file-pdf-o"></span> {tr}Export PDF{/tr}</a></div>
				<div class="p-2"><a href="tiki-slideshow.php?page={$page}&pdf=1&printslides=1" target="_blank"><span class="fa fa-print"></span> {tr}Handouts{/tr}</a></div>
			{/if}

			<div class="p-2"><a href="tiki-index.php?page={$page}"><span class="fa fa-sign-out"></span> {tr}Exit{/tr}</a></div>
		</div>
{else}
			</div>
		</div>
	</div>
{/if}
{if $prefs.feature_bidi eq 'y'}
	</div>
{/if}
{include file='footer.tpl'}
</body>
</html>

